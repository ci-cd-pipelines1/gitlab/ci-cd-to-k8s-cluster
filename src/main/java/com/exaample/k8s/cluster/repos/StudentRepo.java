package com.exaample.k8s.cluster.repos;

import com.exaample.k8s.cluster.entities.Student;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepo extends CrudRepository<Student, Long> {
}

package com.exaample.k8s.cluster.dto.responses;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StudentResponse {
    private Long id;
    private String name;
}

package com.exaample.k8s.cluster.dto.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentRequest {
    private String name;
}

package com.exaample.k8s.cluster.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HardCodedController {

    @GetMapping
    public String get(){
        return "Updated Hello from "+this.getClass().getTypeName();
    }
}

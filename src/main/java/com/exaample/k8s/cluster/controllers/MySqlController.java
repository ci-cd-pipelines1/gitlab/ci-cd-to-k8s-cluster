package com.exaample.k8s.cluster.controllers;

import com.exaample.k8s.cluster.dto.requests.StudentRequest;
import com.exaample.k8s.cluster.dto.responses.StudentResponse;
import com.exaample.k8s.cluster.entities.Student;
import com.exaample.k8s.cluster.repos.StudentRepo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping(value = "mysql")
@Slf4j
public class MySqlController {

    @Autowired
    private StudentRepo repo;

    private static final Logger logger = LoggerFactory.getLogger(MySqlController.class);

    @PostMapping
    public StudentResponse create(@RequestBody StudentRequest request){
        log.debug("MySqlController -> create : operation started ");
        logger.debug("MySqlController -> create : operation started ");
        Student student  = new Student();
        student.setName(request.getName());
        Student savedStudent = repo.save(student);

        StudentResponse response =new StudentResponse();
        response.setId(savedStudent.getId());
        response.setName(savedStudent.getName());

        log.debug("MySqlController -> create : operation completed successfully");
        logger.debug("MySqlController -> create : operation completed successfully");
        return response;
    }

    @GetMapping
    public List<StudentResponse> getAll(){
        log.debug("MySqlController -> getAll : operation started ");
        logger.debug("MySqlController -> getAll : operation started ");
        Iterable<Student> all = repo.findAll();

        List<StudentResponse> responses = new ArrayList<>();
        all.forEach(e ->{
            StudentResponse response =new StudentResponse();
            response.setId(e.getId());
            response.setName(e.getName());
            responses.add(response);
        });

        log.debug("MySqlController -> getAll : operation completed successfully");
        logger.debug("MySqlController -> getAll : operation completed successfully");
        return responses;
    }
}

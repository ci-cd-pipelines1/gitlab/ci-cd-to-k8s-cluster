package com.exaample.k8s.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiCdToK8sClusterApplication {

    public static void main(String[] args) {
        SpringApplication.run(CiCdToK8sClusterApplication.class, args);
    }

}

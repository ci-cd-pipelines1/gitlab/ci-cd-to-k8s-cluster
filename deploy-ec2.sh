set -f
string=$EC2_SERVER
echo $string
ssh ec2-user@$string "mkdir -p ~/ci-cd-to-k8s-cluster"
scp -r k8s-yamls/aws/ ec2-user@$string:~/ci-cd-to-k8s-cluster
ssh ec2-user@$string "cd ./ci-cd-to-k8s-cluster/aws  && kubectl apply -f ."
